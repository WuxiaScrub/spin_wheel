import os
import sys

def rp(relative_path):
    """Get absolute path to resource, works for dev and for PyInstaller"""
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

HOME_PATH = os.getcwd()
RESOURCES_PATH = os.path.join(HOME_PATH, "Resources")

TITLE = "Game"
if os.name == "nt":
    FONT_NAME = rp("freesansbold.ttf")
else:
    FONT_NAME = "freesansbold.ttf"

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GRAY = (128, 128, 128)


# resolution / frames
WIDTH = 1200
HEIGHT = 650
FPS = 60