from sprites import*
from random import*

class Spin_Wheel:
    def __init__(self):
        self.game_width, self.game_height = WIDTH,HEIGHT
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        pg.font.init()
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True


    def new(self, show_menu=True):
        self.all_sprites = pg.sprite.Group()
        self.wheel = Wheel(self, self.game_width//2, self.game_height//2, "wheel_of_fortune_wheel.png", scale=.5)
        self.all_sprites.add(self.wheel)
        self.indicator = Static(self, 270, self.game_height//2, "sprite_indicator.png")
        self.all_sprites.add(self.indicator)
        self.player_action = "waiting"
        self.spin_start_time = 0
        if show_menu:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except Exception as e:
                print(e)


    def update(self):
        self.all_sprites.update()


    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.playing = False
                self.running = False

            if self.wheel.spin_velocity <= 0:
                if event.type == pg.MOUSEBUTTONDOWN and self.player_action == "waiting":
                    self.player_action = "clicking"
                    self.mouse_down_x, self.mouse_down_y = pg.mouse.get_pos()

                if event.type == pg.MOUSEBUTTONUP and self.player_action == "clicking":
                    self.mouse_up_x, self.mouse_up_y = pg.mouse.get_pos()
                    spin_time = (pg.time.get_ticks() - self.spin_start_time)+.5
                    spin_distance = ((self.mouse_up_y-self.mouse_down_y)**2 + (self.mouse_up_x-self.mouse_down_x)**2)**.5
                    if spin_distance/spin_time <= 20:
                        self.wheel.spin_velocity = randrange(20,31) #if too slow, then randomize speed so player can't cheat
                    else:
                        self.wheel.spin_velocity = spin_distance/spin_time
                    self.player_action = "waiting"

                if event.type == pg.MOUSEMOTION and self.player_action == "clicking":
                    self.spin_start_time = pg.time.get_ticks()



            if event.type == pg.KEYDOWN:
                if event.key == pg.K_q:
                    self.running = False
                    self.playing = False


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        self.start_time = pg.time.get_ticks()
                        waiting = False
                        self.running = True


    def show_start_screen(self):
        self.draw_text("Click, drag, and release to spin the wheel.", 18, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press any key to begin.", 18, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


if __name__ == "__main__":
    g = Spin_Wheel()
    g.new()
    pg.quit()